
//třída se zobrazením nové hry
class newGame{
    constructor(){
        newGame.changeView();
    }

    static changeView() {
        const resultEl = document.getElementById("result");
        resultEl.classList.add("hidden");
        const NewGameForm = document.getElementById("startGame");
        NewGameForm.classList.remove("hidden");
    }

}

//třída samostané hry
class Game{

    constructor() {
        this.Difficulty = Game.getDificulty();
        this.Size = Game.getSize();
        this.box = Game.getBox(this.Size);
        this.time = Game.getTimeFromDif(this.Difficulty);
        this.game = null;
        this.score = 0;

        const canvas = document.getElementById("game");
        this.ctx = canvas.getContext("2d");
        this.Width = canvas.offsetWidth;
        this.Height = canvas.offsetHeight;

        //prezentace hada
        this.snake = [];
        this.snake[0] = {
            x : this.box * ((this.Width/this.box)/2),
            y : this.box * ((this.Height/this.box)/2)
        };

        //prezentace jídla, pozice je náhodně generovaná
        this.food = {
            x : Math.floor(Math.random()*(this.Width/this.box)) * this.box,
            y : Math.floor(Math.random()*(this.Height/this.box)) * this.box
        };

        document.getElementById("backToNewGame").addEventListener("click", () => {
            clearInterval(this.game);
            new newGame();
        });
        document.addEventListener("keydown", e => this.keyPress(e));
        window.addEventListener("keydown", function(e) {
            // space and arrow keys
            if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
                e.preventDefault();
            }
        }, false);

        this.direction = "";

        Game.changeView();
    }



    //zobrazení hrací plochy
    static changeView() {
        const NewGameForm = document.getElementById("startGame");
        NewGameForm.classList.add("hidden");
    }

    //získání obtížnosti hry
    static getDificulty() {
        let Difficulty = "";
        const Difficulties = document.getElementsByClassName("difficulty");

        for (let i = 0; i < Difficulties.length; i++) {
            if (Difficulties[i].checked){
                Difficulty = Difficulties[i].value;
            }
        }

        return Difficulty;
    }

    //získání elikosti hrací plochy
    static getSize() {
        let Size = "";
        const Sizes = document.getElementsByClassName("size");

        for (let i = 0; i < Sizes.length; i++) {
            if (Sizes[i].checked){
                Size = Sizes[i].value;
            }
        }

        return Size;
    }

    //získání hodnoty obnovovací frekvence hry
    static getTimeFromDif(Difficulty){
        let time = 0;
        if (Difficulty === "beginner"){
            time = 1000/5;
        }

        if (Difficulty === "medium"){
            time = 1000/15;
        }

        if (Difficulty === "hard"){
            time = 1000/25;
        }
        return time;
    }

    //získání velikosti boxu, z kterého se zkhládá hrací plocha
    static getBox(Size){
        let box = 0;
        if (Size === "small"){
            box = 50;
        }

        if (Size === "medium"){
            box = 30;
        }

        if (Size === "big"){
            box = 10;
        }
        return box;
    }

    //zjištění jaká klávesa byla zmáčknuta
    keyPress(event) {

        if (event.key === "ArrowUp" && this.direction !== "DOWN"){
            this.direction = "UP";
        }

        if (event.key === "ArrowDown" && this.direction !== "UP"){
            this.direction = "DOWN";
        }

        if (event.key === "ArrowLeft" && this.direction !== "RIGHT"){
            this.direction = "LEFT";
        }

        if (event.key === "ArrowRight" && this.direction !== "LEFT"){
            this.direction = "RIGHT";
        }

    }

    //vykreslení hrací plochy
    drawCtx(ctx) {
        ctx.fillStyle = document.getElementById("backgroundColor").value;
        ctx.fillRect(0, 0, this.Width, this.Height);
        ctx.strokeStyle = "red";
        ctx.strokeRect(0, 0, this.Width, this.Height);
    }

    //vykreslení hada
    drawSnake(ctx) {

        for( let i = 0; i < this.snake.length ; i++){
            ctx.fillStyle =  document.getElementById("colorSnake").value;
            ctx.fillRect(this.snake[i].x, this.snake[i].y, this.box, this.box);

        }

        ctx.beginPath();
        ctx.arc(this.snake[0].x+this.box/4, this.snake[0].y+this.box/4, this.box/8, 0, 2 * Math.PI);
        ctx.fillStyle = "white";
        ctx.fill();

        ctx.beginPath();
        ctx.arc(this.snake[0].x+((this.box/4)*3), this.snake[0].y+this.box/4, this.box/8, 0, 2 * Math.PI);
        ctx.fillStyle = "white";
        ctx.fill();

    }

    //vykreslení jablka
    drawFood(ctx) {
        const apple = document.querySelector("img");
        ctx.drawImage(apple, this.food.x, this.food.y, this.box, this.box);
    }

    //změna hlavy na novou pozici
    changeHead(Head) {

        if (this.direction === "UP"){

            Head.y -= this.box;

            if (Head.y < 0){
                Head.y = this.Height-this.box;
            }
        }

        if (this.direction === "DOWN"){

            Head.y += this.box;

            if (Head.y >= this.Height){
                Head.y = 0;
            }
        }

        if (this.direction === "LEFT"){

            Head.x -= this.box;

            if (Head.x < 0){
                Head.x = this.Width-this.box;
            }
        }

        if (this.direction === "RIGHT"){

            Head.x += this.box;

            if (Head.x >= this.Width){
                Head.x = 0;
            }
        }

    }

    //zjištění, jestli nová hlava hada zasahuje do těla
    static collision(head, array){
        for(let i = 0; i < array.length; i++){
            if(head.x === array[i].x && head.y === array[i].y){
                return true;
            }
        }
        return false;
    }

    //zjištění, jestli nová hlava snědla jablko
    static eat(Head, food) {
        return Head.x === food.x && Head.y === food.y;
    }

    //vypsání skóre do html
    static writeScore(score, ScoreEl) {
        ScoreEl.innerHTML = "";
        ScoreEl.innerHTML = `<h4>${score}</h4>`;
    }

    //hlavní funkce, která se opakuje a vykresluje hada
    draw() {
        this.drawCtx(this.ctx);
        this.drawSnake(this.ctx);
        this.drawFood(this.ctx);

        let NewHead = {
            x : this.snake[0].x,
            y : this.snake[0].y
        };

        this.changeHead(NewHead);

        if(Game.eat(NewHead, this.food)){
            this.score++;
            new Audio("sound/eat.mp3").play();
            this.food = {
                x : Math.floor(Math.random()*(this.Width/this.box)) * this.box,
                y : Math.floor(Math.random()*(this.Height/this.box)) * this.box
            };
        }else{
            this.snake.pop();
        }

        if (Game.collision(NewHead, this.snake)){
            new Audio("sound/gameOver.mp3").play();
            clearInterval(this.game);

            new Result(document.getElementById("name").value, this.score, this.Difficulty, this.Size);
        }

        this.snake.unshift(NewHead);

        Game.writeScore(this.score, document.getElementById("score"));
    }

    //získání času
    getTime(){
        return this.time;
    }

    //nastavení hry
    setGame(game){
        this.game = game;
    }
}

//třída representující stránku s výsledky
class Result{
    constructor(name, score, difficulty, size) {
        this.lastResult = {
            name: name,
            score: score,
            difficulty: difficulty,
            size: size
        };

        document.getElementById("newGame").addEventListener("click", () =>  new newGame());
        document.getElementById("resetScore").addEventListener("click", () => this.resetScore());



        this.changeView();
        Result.saveResult(this.lastResult);
    }

    //změna obrazovek a zobrazení výsledkové stránky
    changeView() {
        document.body.classList.add('modal-visible');
        document.getElementById("result").classList.remove("hidden");
        const lastGame = document.getElementById("lastGame");
        lastGame.innerHTML = `<h4>Player name: ${this.lastResult.name}</h4>
                            <h4>Score: ${this.lastResult.score}</h4>
                            <h4>Difficulty: ${this.lastResult.difficulty}</h4>
                            <h4>Size map: ${this.lastResult.size}</h4>`;
    }

    //uložení výsledků do LocalStorage
    static saveResult(lastResult) {
        let Results = JSON.parse(localStorage.getItem("results"));

        if (Results){
            Results.unshift(lastResult);
        } else {
            let newResult = [];
            newResult.unshift(lastResult);
            Results = newResult;
        }

        if (Results.length > 10){
            Results.pop();
        }

        Result.writeResultTable(Results);

        localStorage.setItem("results", JSON.stringify(Results));
    }

    //vypsání výsledku z LocalStorage do tabulky
    static writeResultTable(Results) {
        const table = document.getElementById('resultTable').getElementsByTagName('tbody')[0];
        table.innerHTML = "";

        for (let i = 0; i < Results.length; i++) {
            let newRow   = table.insertRow(table.rows.length);

            let nameCell  = newRow.insertCell(0);
            let scoreCell  = newRow.insertCell(1);
            let difficultyCell  = newRow.insertCell(2);
            let sizeCell  = newRow.insertCell(3);

            let nameText  = document.createTextNode(Results[i].name);
            let scoreText  = document.createTextNode(Results[i].score);
            let difficultyText  = document.createTextNode(Results[i].difficulty);
            let sizeText  = document.createTextNode(Results[i].size);
            nameCell.appendChild(nameText);
            scoreCell.appendChild(scoreText);
            difficultyCell.appendChild(difficultyText);
            sizeCell.appendChild(sizeText);
        }
    }

    //vyresetování LocalStorage
    resetScore() {
        localStorage.clear();
        const table = document.getElementById('resultTable').getElementsByTagName('tbody')[0];
        table.innerHTML = "";
        new Result(this.lastResult.name, this.lastResult.score, this.lastResult.difficulty, this.lastResult.size);
    }
}


//funkce na spuštění hry
function startGame() {
    let game = new Game();
    let time = game.getTime();
    game.setGame(setInterval(game.draw.bind(game), time));
    return false;
}
