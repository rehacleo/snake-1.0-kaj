# Semestrální práce na předmět KAJ
## Hra Snake
### Zadání
Jako semestrální práci z předmětu Vývoj klientských aplikací v Javascriptu (KAJ) jsem si vybral hru Snake. Kde cílem hry je sníst co nejvíce jídla, a přitom nenarazit do „svého“ těla. Jedná se o single-page aplikaci, která funguje i bez připojení na internet.

### Použité technologie
-	Local Storage API - ukládání výsledků her
-	Canvas - zobrazení hrací plochy a hada
-	Media API - přehrávání zvuků
-	Media Queries - zaručení, že se canvas vvejde i na mobilu
-	Flex - použití na responzivní vzhled

### Popis funkčnosti
Po spuštění aplikace se zobrazí stránka s novou hrou. Zde musí uživatel zadat nickname. Dále si může zvolit ze 3 obtížností hry a ze 3 velikostí hrací plochy. Uživatel si může určit i jakou barvu bude mít had a jakou barvu bude mít hrací plocha. 

Po kliknutí na tlačítko Play se zobrazí okno s hrací plochou, hadem a jablkem. Nad hrací plochou se nachází aktuální skóre a tlačítko zpět na novou hru. Hra v tuto chvíli čeká, až uživatel stiskne jednu ze šipek na klávesnici. Po stisknutí šipky hra začne. V této hře je možné projíždět i skrze kraje hrací plochy. Po snědení jablka had povyroste o jeden díl své délky. Hra končí, když hlava hada narazí do zbytku svého těla.

Po narazení do svého těla hra končí a zobrazí se stránka s výsledky hry. Na stránce je vidět nickname hráče, jeho dosažené skóre, jakou měl obtížnost a jak velkou hrací plochu. Dále je zde zobrazeno posledních deset her, které jsou uložené v paměti prohlížeče. Historie posledních her je možné resetovat. Na stránce se také nachází tlačítko zpět na novou hru.


